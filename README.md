# Eclipse Object Teams

Eclipse Object Teams adds new dimensions of modularity for extreme re-use while
sustaining a crisp architecture that's a breeze to maintain during long-term
software evolution. — This is done by extending object oriented programming
with the concept of Roles which are grouped into Teams. — The Object Teams
Development Tooling supports efficient development using OT/J by seamlessly and
comprehensively extending JDT.

## Contributing

[Contributions are always welcome!](https://gitlab.eclipse.org/eclipse/objectteams/objectteams/-/blob/master/CONTRIBUTING.md)

## License

[Eclipse Public License (EPL) v2.0](https://www.eclipse.org/legal/epl-2.0/)

## Links

### Project Page
* https://projects.eclipse.org/projects/tools.objectteams

### Literatur
* http://www.objectteams.org/publications/

### Wiki
* [Introduction to OT/J](http://wiki.eclipse.org/OTJ) — the language
* [Object Teams Development Tooling (OTDT)](http://wiki.eclipse.org/Object_Teams_Development_Tooling) — the IDE
* [OT/Equinox](http://wiki.eclipse.org/Category:OTEquinox) — the module system
* [List of all Wiki pages](http://wiki.eclipse.org/Category:Object_Teams) on Object Teams

### Help pages 

_Help pages are not currently available online, only available inside an OTDT installation_
