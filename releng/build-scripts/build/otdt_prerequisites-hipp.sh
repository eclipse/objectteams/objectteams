###############################################################################
# Copyright (c) 2010, 2016 Stephan Herrmann and others.
# This program and the accompanying materials
# are made available under the terms of the Eclipse Public License 2.0
# which accompanies this distribution, and is available at
# https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
#
# Contributors:
#     Stephan Herrmann - initial API and implementation
###############################################################################

# Environment variables for the script otdt_runtests.sh

# EXPORT: Root location on build.eclipse.org:
BASEDIR=`pwd`

# EXPORT: root directory for building and testing:
OT_TESTSUITE_DIR=${BASEDIR}/testrun

# EXPORT: tmp directory for logging:
TMPDIR="${BASEDIR}/tmp"

# EXPORT: tmp directory for test files:
#TEST_TMPDIR="${HOME}/tmp"
TEST_TMPDIR=${JAVA_TMPDIR}

# EXPORT: directory for metadata from previous builds:
METADATA="${BASEDIR}/metadata"

# EXPORT:
FETCH_CACHE_LOCATION=${HOME}/gitCache

# EXPORT:
MAP_FILE_PATH=${BASEDIR}/releng/map/otdt.map

# Base dir for finding previous platform build:
DROPS4=https://download.eclipse.org/eclipse/downloads/drops4

# Configure ANT:
ANT_HOME=/shared/common/apache-ant-1.10.5/
PATH=${ANT_HOME}/bin:${PATH}

# Configure Java:
JAVA_HOME=${JAVA_HOME:="/shared/common/jdk-9_x64-latest"}
PATH=${JAVA_HOME}/bin:${PATH}

# EXPORT: Nice-level for the Ant process:
NICE="10"

# Architecture (as used by OSGi):
ARCH=`arch`

# VERSIONS:
# Eclipse SDK build identifier (used for substitution in otdt.map.in etc.):
SDK_QUALIFIER=${SDK_QUALIFIER:="I20250228-0140"}

# EXPORT: URL for installing the test framework from:
SDK_P2_URL=https://download.eclipse.org/eclipse/updates/4.35-I-builds/${SDK_QUALIFIER}

# used only locally (components of the ECLIPSE_SDK_TGZ path):
EVERSION=${EVERSION:="4.35RC2"}
DROP=${DROPS4}/${DROP:="S-4.35RC2-202502280140"}

# EXPORT: archive file of the base eclipse SDK build:
ECLIPSE_SDK_TGZ=${DROP}/eclipse-SDK-${EVERSION}-linux-gtk-${ARCH}.tar.gz

# EXPORT: where to find previously published plugins&features:
PUBLISHED_UPDATES=${HOME}/downloads/objectteams/updates/ot2.8/202307181937

