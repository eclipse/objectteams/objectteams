/**********************************************************************
 * This file is part of "Object Teams Development Tooling"-Software
 * 
 * Copyright 2007 Fraunhofer Gesellschaft, Munich, Germany,
 * for its Fraunhofer Institute for Computer Architecture and Software
 * Technology (FIRST), Berlin, Germany and Technical University Berlin,
 * Germany.
 * 
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Please visit http://www.eclipse.org/objectteams for updates and contact.
 * 
 * Contributors:
 * Fraunhofer FIRST - Initial API and implementation
 * Technical University Berlin - Initial API and implementation
 **********************************************************************/
package org.eclipse.objectteams.otdt.internal.compiler.adaptor;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.IAccessRule;
import org.eclipse.jdt.core.IClasspathAttribute;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.compiler.IProblem;
import org.eclipse.jdt.internal.core.ClasspathAccessRule;
import org.eclipse.osgi.service.resolver.BundleDescription;
import org.eclipse.osgi.service.resolver.ExportPackageDescription;
import org.eclipse.osgi.service.resolver.StateHelper;
import org.eclipse.pde.core.plugin.IPluginModelBase;
import org.eclipse.pde.internal.core.ibundle.IBundlePluginModelBase;

import base org.eclipse.pde.internal.core.PDEClasspathContainer;
import base org.eclipse.pde.internal.core.RequiredPluginsClasspathContainer;
import base org.eclipse.pde.internal.core.PDEClasspathContainer.Rule;
import base org.eclipse.pde.internal.core.bundle.BundlePluginModel;
import base org.eclipse.pde.internal.core.plugin.WorkspaceExtensionsModel;

/**
 * Adapt classes from the PDE core as to feed information about aspectBindings
 * into the compilation process (to be consumed by BaseImportChecker).
 * 
 * Final target as expected by the BaseImportChecker:
 *  + aspectBindingData (of type AdaptedBaseBundle) have been added to ClasspathAccessRules 
 *    and the problemID has been adjusted.
 * 
 * 
 * @author stephan
 * @since 1.1.5
 */
@SuppressWarnings("restriction")
public team class PDEAdaptor 
{
	static PDEAdaptor instance;
	
	protected ThreadLocal<String> currentCPEntryPath = new ThreadLocal<>(); 
	protected ThreadLocal<BundleDescription> currentBundle = new ThreadLocal<>();

	public PDEAdaptor() {
		instance= this;
	}

	/**
	 * <ul> 
	 * <li>Store aspectBinding info in Role objects.</li>
	 * <li>Add additional rules for forcedExports.</li>
	 * </ul> 
	 */
	protected class RequiredPluginsClasspathContainer
			extends PDEClasspathContainer
			playedBy RequiredPluginsClasspathContainer 
	{
		
		protected AspectBindingReader aspectBindingReader;

		@SuppressWarnings("decapsulation")
		captureBundleName <- replace addPlugin;
		callin boolean captureBundleName(BundleDescription desc) throws CoreException {
			PDEAdaptor.this.currentBundle.set(desc);
			try {
				return base.captureBundleName(desc);
			} finally {
				PDEAdaptor.this.currentBundle.remove();
			}
		}

		
		void updateRule(String providingBundle, Rule rule) 
		 <- after Rule getRule(StateHelper helper, BundleDescription desc, ExportPackageDescription export)
			with { providingBundle <- export.getExporter().getSymbolicName(),
				   rule            <- result
			}
		/** Handles adaptation info for exported packages, Rule role created via regular lifting. */
		void updateRule(String providingBundle, Rule rule) {
			if (aspectBindingReader != null && aspectBindingReader.isAdaptedBase(providingBundle))
				rule.addData(providingBundle, aspectBindingReader.getAdaptationInfo(providingBundle));
		}
		
		@SuppressWarnings("decapsulation")
		List<org.eclipse.pde.internal.core.PDEClasspathContainer.Rule> addForcedExports(IPluginModelBase desc) 
		 <- replace List<org.eclipse.pde.internal.core.PDEClasspathContainer.Rule> getInclusions(Map<BundleDescription, List<Rule>> map, IPluginModelBase desc)
		    with { desc <- desc }
		/** Handles adaptation info for non-exported packages, Rule role explicitly created. */		
		@SuppressWarnings("decapsulation")
		callin List<org.eclipse.pde.internal.core.PDEClasspathContainer.Rule> addForcedExports(IPluginModelBase desc) 
		{
			List<org.eclipse.pde.internal.core.PDEClasspathContainer.Rule> regularRules= base.addForcedExports(desc);
			if (aspectBindingReader == null)
				return regularRules; // done: no aspect bindings
			String symbolicName = desc.getBundleDescription().getSymbolicName();
			HashSet<String> forcedExports= aspectBindingReader.getForcedExports(symbolicName);
			if (forcedExports == null)
				return regularRules; // done: no forced exports
			
			AdaptedBaseBundle aspectBindingData= aspectBindingReader.getAdaptationInfo(symbolicName);			
			// create additional rules:
			List<org.eclipse.pde.internal.core.PDEClasspathContainer.Rule> result= new ArrayList<>();
			for (String forcedExport : forcedExports) 
				result.add(new Rule(aspectBindingData, forcedExport));
			
			// merge:
			result.addAll(regularRules);
			
			return result;
		}
		
		@SuppressWarnings("decapsulation")
		protected
		BundleModel getBundleModel() -> get IPluginModelBase fModel
			with { result <- (BundlePluginModel)fModel }

		// -- debug: --		
		String baseToString() -> String toString();
		
		@Override
		public String toString() {
			return "Role for "+baseToString()+" with aspectBindingReader\n  "
				  + ((this.aspectBindingReader != null) ? this.aspectBindingReader.toString() : "null");
		}
	}
	
	/**
	 * Synthetic rules representing adapted or forcedExports.
	 */
	@SuppressWarnings("decapsulation")
	protected class Rule playedBy Rule
	{
		void setPath(IPath path) -> set IPath path;

		// intermediate storage between AspectBindingReader and ClasspathAccessRule:
		protected Map<String,AdaptedBaseBundle> aspectBindingData; 
		protected boolean isForcedExport;
		
		/** Ctor for force-exported packages (merely adapted packages instantiate via lifting ctor). */
		protected Rule(AdaptedBaseBundle aspectBindingData, String packageName) 
		{
			base(new Path(packageName.replace('.', '/')+"/*"), false); //$NON-NLS-1$
			this.aspectBindingData= new HashMap<>();
			this.aspectBindingData.put(aspectBindingData.symbolicName, aspectBindingData);
			this.isForcedExport= true;
		}
		// -- debug: --
		String baseToString() -> String toString();
		
		@Override
		public String toString() {
			String result= baseToString();
			if (this.isForcedExport)
				result+= " (forced export)";
			return result+" with aspect data\n  "
				  + ((this.aspectBindingData == null) ? "null" : this.aspectBindingData.toString());
		}

		public void addData(String baseBundle, AdaptedBaseBundle data) {
			if (this.aspectBindingData == null)
				this.aspectBindingData = new HashMap<>();
			this.aspectBindingData.put(baseBundle, data);
		}
	}
	
	/** After converting Rules to IAccessRules transfer adaptation info and adjust problemId. */
	protected class PDEClasspathContainer playedBy PDEClasspathContainer 
	{
		rememberProjectPath <- replace addProjectEntry;
		callin void rememberProjectPath(IProject project) throws CoreException {
			PDEAdaptor.this.currentCPEntryPath.set(project.getName());
			try {
				base.rememberProjectPath(project);
			} finally {
				PDEAdaptor.this.currentCPEntryPath.remove();;
			}
		}

		rememberLibraryPath <- replace addLibraryEntry;
		static callin void rememberLibraryPath(IPath path) {
			PDEAdaptor.this.currentCPEntryPath.set(path.toString());
			try {
				base.rememberLibraryPath(path);
			} finally {
				PDEAdaptor.this.currentCPEntryPath.remove();
			}
		}
	
		void getAccessRules(Rule[] rules, IAccessRule[] accessRules) 
		  <- after IAccessRule[] getAccessRules(List<Rule> rules)
			 with { rules <- rules.toArray(new Rule[rules.size()]), accessRules <- result }
		static void getAccessRules(Rule[] rules, IAccessRule[] accessRules) {
			BundleDescription currentBundle = PDEAdaptor.this.currentBundle.get();
			for (int i = 0; i < rules.length; i++) {
				Rule rule = rules[i];
				if (rule.aspectBindingData != null) {
					for (AdaptedBaseBundle data : rule.aspectBindingData.values()) {
						setCPentryPathToBaseBundleData(currentBundle, data);
						ClasspathAccessRule classpathAccessRule = (ClasspathAccessRule)accessRules[i];
						if (rule.isForcedExport) {
							// don't let this rule leak to other clients
							classpathAccessRule = new ClasspathAccessRule(classpathAccessRule.pattern, IProblem.BaseclassDecapsulationForcedExport);
							classpathAccessRule.aspectBindingData = new Object[] { data };
							accessRules[i] = classpathAccessRule;
						} else { 
							addAspectBindingData(classpathAccessRule, data);
						}
					}
				}
			}
		}
		protected static void setCPentryPathToBaseBundleData(BundleDescription currentBundle, AdaptedBaseBundle data) {
			String symbolicName = data.symbolicName;
			if (symbolicName.equals(currentBundle.getSymbolicName())
					|| (currentBundle.getHost() != null && symbolicName.equals(currentBundle.getHost().getName()))) {
				String currentPath = PDEAdaptor.this.currentCPEntryPath.get();
				if (currentPath != null) {
					data.cpEntryPaths.add(currentPath);
				}
			}
		}
	}
	/**
     * Add the given aspect binding data to the given access rule.
     * @return: has data been added (vs. merged or already present)? 
	 */
	public static boolean addAspectBindingData(ClasspathAccessRule accessRule, AdaptedBaseBundle aspectBindingData) {
		// nothing present yet?
		if (accessRule.aspectBindingData == null) {
			accessRule.aspectBindingData = new Object[] { aspectBindingData };
			if (accessRule.problemId == 0)
				accessRule.problemId= IProblem.AdaptedPluginAccess;
			return true;
		}
		// exact binding data already present?
		for (Object data : accessRule.aspectBindingData)
			if (data == aspectBindingData)
				return false;
		// different binding data for the same base bundle present?
		for (Object data : accessRule.aspectBindingData)
			if (((AdaptedBaseBundle)data).merge(aspectBindingData))
				return false;
		// different base bundles, add it:
		int len = accessRule.aspectBindingData.length;
		Object[] newAspectBindingData = new Object[len+1];
		System.arraycopy(accessRule.aspectBindingData, 0, newAspectBindingData, 0, len);
		newAspectBindingData[len] = aspectBindingData;
		accessRule.aspectBindingData = newAspectBindingData;
		return true;
	}

	/** Helper role for updating aspect binding information. */
	protected class BundleModel playedBy BundlePluginModel {
		protected AspectBindingReader aspectBindingReader;
		
	}
	
	/** 
	 * This role listens to updates on its base.
	 * If the associated bundle model has a role with a registered
	 * aspect binding reader, trigger reloading when the model has changed. 
	 */
	protected class ModelListener playedBy WorkspaceExtensionsModel {
		void resetAspectReader() <- after void load(InputStream is, boolean reload);
		void resetAspectReader () {
			try {
				BundleModel bundle= getFBundleModel();
				if (bundle != null && bundle.aspectBindingReader != null)
					bundle.aspectBindingReader.reload();
			} catch (ClassCastException cce) {
				// CCE could be thrown by parameter mapping of getFBundleModel().
			}
		}
		/** This declaration is for documentation only: read the fBundleModel field.
		 * @return a BundleModel role
		 * @throws ClassCastException thrown when fBundleModel is not a BundlePluginModel.
		 */
		abstract BundleModel getFBundleModel() throws ClassCastException;
		@SuppressWarnings("decapsulation")
		BundleModel getFBundleModel() -> get IBundlePluginModelBase fBundleModel
			with { result <- (BundlePluginModel)fBundleModel }
	}

	/** Register an aspect binding reader for a given RequiredPluginsClasspathContainer. */
	void setAspectBindingReader(AspectBindingReader aspectBindingReader,
		RequiredPluginsClasspathContainer as RequiredPluginsClasspathContainer container) 
	{
		container.aspectBindingReader= aspectBindingReader;
		try {
			// link bundle model and reader for updating lateron:
			BundleModel bundle= container.getBundleModel();
			if (bundle != null)
				bundle.aspectBindingReader= aspectBindingReader;
		} catch (ClassCastException cce) {
			// can happen in param mapping of c-t-f, wrong model type, ignore.
		}
		for (Rule rule : this.getAllRoles(Rule.class)) {
			this.unregisterRole(rule);
		}
	}
}
