/**********************************************************************
 * This file is part of "Object Teams Development Tooling"-Software
 * 
 * Copyright 2007, 2018 Technical University Berlin, Germany and others.
 * 
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Please visit http://www.eclipse.org/objectteams for updates and contact.
 * 
 * Contributors:
 * 	   Technical University Berlin - Initial API and implementation
 *     IBM Corporation - implementation of individual method bodies
 **********************************************************************/
team package org.eclipse.objectteams.otdt.internal.ui.assist.CompletionAdaptor;


import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.Modifier;

/**
 * Add new functions to StubUtility2Core, accessing otherwise invisible helper functions.
 * @author stephan
 * @since 2.7.2 (separated out from sibling StubUtility2 due to code move from jdt.ui to jdt.core.manipulation)
 */
@SuppressWarnings({ "decapsulation"/*final baseclass*/})
protected class StubUtility2Core playedBy StubUtility2Core
{
	isStatic <- replace isStatic;

	@SuppressWarnings("basecall")
	static callin boolean isStatic(IMethodBinding method) {
		if (Modifier.isStatic(method.getModifiers())) {
			// our base is interested only in 'normal' static methods, not counting abstract static role methods
			return !Modifier.isAbstract(method.getModifiers());
		}
		return false;
	}
}

