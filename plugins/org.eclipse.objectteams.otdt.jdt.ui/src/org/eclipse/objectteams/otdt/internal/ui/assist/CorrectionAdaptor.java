/*******************************************************************************
 * This file is part of "Object Teams Development Tooling"-Software
 * 
 * Copyright 2007 Technical University Berlin, Germany and others.
 * 
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Please visit http://www.eclipse.org/objectteams for updates and contact.
 * 
 * Contributors:
 *     Technical University Berlin - Initial API and implementation
 *     IBM Corporation - copies of individual methods from bound base classes.
 *******************************************************************************/
package org.eclipse.objectteams.otdt.internal.ui.assist;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ClassInstanceCreation;
import org.eclipse.jdt.core.dom.CompilationUnit;
import org.eclipse.jdt.core.dom.ConstructorInvocation;
import org.eclipse.jdt.core.dom.EnumConstantDeclaration;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.Modifier;
import org.eclipse.jdt.core.dom.SuperConstructorInvocation;
import org.eclipse.jdt.core.dom.TSuperConstructorInvocation;
import org.eclipse.jdt.internal.corext.dom.Bindings;
import org.eclipse.jdt.internal.corext.util.Messages;
import org.eclipse.jdt.internal.ui.text.correction.ASTResolving;
import org.eclipse.jdt.internal.ui.text.correction.CorrectionMessages;
import org.eclipse.jdt.internal.ui.text.correction.IProposalRelevance;
import org.eclipse.jdt.internal.ui.text.correction.proposals.NewAbstractMethodCorrectionProposalCore;
import org.eclipse.jdt.internal.ui.text.correction.proposals.NewMethodCorrectionProposalCore;
import org.eclipse.jdt.internal.ui.viewsupport.BasicElementLabels;
import org.eclipse.jdt.ui.text.java.IInvocationContext;
import org.eclipse.jdt.ui.text.java.IProblemLocation;

import base org.eclipse.jdt.internal.ui.text.correction.UnresolvedElementsBaseSubProcessor;

/**
 * Individual method bodies are copied from their base version.
 * 
 * @author stephan
 * @since 1.1.2
 */
@SuppressWarnings("restriction")
public team class CorrectionAdaptor {
	protected class UnresolvedElementsBaseSubProcessor<P> playedBy UnresolvedElementsBaseSubProcessor<P>
	{
		/** This callin adjusts the compilation unit, when a new method should be inserted
		 *  into the outer type: which for a role file is NOT within the current CU.
		 */	
		@SuppressWarnings("decapsulation")
		void addNewMethodProposals(ICompilationUnit cu, CompilationUnit astRoot, Expression sender, List<Expression> arguments, boolean isSuperInvocation, ASTNode invocationNode, String methodName, Collection<P> proposals)
		<- replace void addNewMethodProposals(ICompilationUnit cu, CompilationUnit astRoot, Expression sender, List<Expression> arguments, boolean isSuperInvocation, ASTNode invocationNode, String methodName, Collection<P> proposals);
		
		@SuppressWarnings("decapsulation")
		protected ITypeBinding[] getParameterTypes(List<Expression> args) -> ITypeBinding[] getParameterTypes(List<Expression> args);
		
		@SuppressWarnings("decapsulation")
		protected void addParameterMissmatchProposals(IInvocationContext arg0, IProblemLocation arg1, List<IMethodBinding> arg2, ASTNode arg3, List<Expression> arg4, Collection<P> arg5) 
			-> void addParameterMissmatchProposals(IInvocationContext arg0, IProblemLocation arg1, List<IMethodBinding> arg2, ASTNode arg3, List<Expression> arg4, Collection<P> arg5);

		@SuppressWarnings({ "basecall", "inferredcallout", "decapsulation" }) // decapsulation in inferred callout
		callin void addNewMethodProposals(ICompilationUnit cu, CompilationUnit astRoot, Expression sender, List<Expression> arguments, boolean isSuperInvocation, ASTNode invocationNode, String methodName, Collection<P> proposals) 
				throws JavaModelException
		{
			// OT_COPY_PASTE:
			ITypeBinding nodeParentType= Bindings.getBindingOfParentType(invocationNode);
			ITypeBinding binding= null;
			if (sender != null) {
				binding= sender.resolveTypeBinding();
			} else {
				binding= nodeParentType;
				if (isSuperInvocation && binding != null) {
					binding= binding.getSuperclass();
				}
			}
			if (binding != null && binding.isFromSource()) {
				ITypeBinding senderDeclBinding= binding.getErasure().getTypeDeclaration();
//{ObjectTeams: don't erase a RoleTypeBinding:
				if (binding.isRole()) {
					senderDeclBinding = binding.getTypeDeclaration();
				}
// SH}

				ICompilationUnit targetCU= ASTResolving.findCompilationUnitForBinding(cu, astRoot, senderDeclBinding);
				if (targetCU != null) {
					String label;
					String labelAbstract;
					ITypeBinding[] parameterTypes= getParameterTypes(arguments);
					if (parameterTypes != null) {
						String sig= ASTResolving.getMethodSignature(methodName, parameterTypes, false);
						boolean isSenderTypeAbstractClass = (senderDeclBinding.getModifiers() &  Modifier.ABSTRACT) > 0; 
						boolean isSenderBindingInterface= senderDeclBinding.isInterface();
						int firstProposalUid= -1;
						if (nodeParentType == senderDeclBinding) {
							label= Messages.format(CorrectionMessages.UnresolvedElementsSubProcessor_createmethod_description, sig);
							labelAbstract= Messages.format(CorrectionMessages.UnresolvedElementsSubProcessor_createmethod_abstract_description, sig);
							if (isSenderBindingInterface) {
								firstProposalUid= NewMethodProposal1;
							} else {
								firstProposalUid= NewMethodProposal2;
							}
						} else {
							firstProposalUid= NewMethodProposal3;
							label= Messages.format(CorrectionMessages.UnresolvedElementsSubProcessor_createmethod_other_description, new Object[] { sig, BasicElementLabels.getJavaElementName(senderDeclBinding.getName()) } );
							labelAbstract= Messages.format(CorrectionMessages.UnresolvedElementsSubProcessor_createmethod_abstract_other_description, new Object[] { sig, BasicElementLabels.getJavaElementName(senderDeclBinding.getName()) } );
						}
						{
							NewMethodCorrectionProposalCore core= new NewMethodCorrectionProposalCore(label, targetCU, invocationNode, arguments, senderDeclBinding, IProposalRelevance.CREATE_METHOD);
							P t= newMethodProposalToT(core, firstProposalUid);
							if (t != null)
								proposals.add(t);
						}

						if (isSenderTypeAbstractClass ) {
							NewAbstractMethodCorrectionProposalCore core= new NewAbstractMethodCorrectionProposalCore(labelAbstract, targetCU, invocationNode, arguments, senderDeclBinding, IProposalRelevance.CREATE_METHOD);
							P t= newMethodProposalToT(core, NewMethodProposal4);
							if (t != null)
								proposals.add(t);
						}


						if (senderDeclBinding.isNested() && cu.equals(targetCU) && sender == null && Bindings.findMethodInHierarchy(senderDeclBinding, methodName, (ITypeBinding[]) null) == null) { // no covering method
							ASTNode anonymDecl= astRoot.findDeclaringNode(senderDeclBinding);
							if (anonymDecl != null) {
								senderDeclBinding= Bindings.getBindingOfParentType(anonymDecl.getParent());
								isSenderBindingInterface= senderDeclBinding.isInterface();
								isSenderTypeAbstractClass= (senderDeclBinding.getModifiers() &  Modifier.ABSTRACT) > 0;
								if (!senderDeclBinding.isAnonymous()) {
									String[] args= new String[] { sig, ASTResolving.getTypeSignature(senderDeclBinding) };
									label= Messages.format(CorrectionMessages.UnresolvedElementsSubProcessor_createmethod_other_description, args);
									labelAbstract= Messages.format(CorrectionMessages.UnresolvedElementsSubProcessor_createmethod_abstract_other_description, args);
									int nextUid;
									if (isSenderBindingInterface) {
										nextUid= NewMethodProposal5;
									} else {
										nextUid= NewMethodProposal6;
									}
//{ObjectTeams: the pay-load: when traveling out of a role file, search for the new targetCU:
									if (binding.isRole() && astRoot.findDeclaringNode(senderDeclBinding) == null)
										targetCU= ASTResolving.findCompilationUnitForBinding(cu, astRoot, senderDeclBinding);
// SH}
									NewMethodCorrectionProposalCore core= new NewMethodCorrectionProposalCore(label, targetCU, invocationNode, arguments, senderDeclBinding, IProposalRelevance.CREATE_METHOD);
									P t= newMethodProposalToT(core, nextUid);
									if (t != null)
										proposals.add(t);
									if ( isSenderTypeAbstractClass ) {
										NewAbstractMethodCorrectionProposalCore c2= new NewAbstractMethodCorrectionProposalCore(labelAbstract, targetCU, invocationNode, arguments, senderDeclBinding, IProposalRelevance.CREATE_METHOD);
										P t2= newMethodProposalToT(c2, NewMethodProposal7);
										if (t2 != null)
											proposals.add(t2);
									}
								}
							}
						}
					}
				}
			}
		}

		@SuppressWarnings("decapsulation")
		P newMethodProposalToT(NewMethodCorrectionProposalCore core, int uid) -> P newMethodProposalToT(NewMethodCorrectionProposalCore core, int uid);
		
		void getConstructorProposals(IInvocationContext context, IProblemLocation problem, Collection<P> proposals) 
			<- replace void collectConstructorProposals(IInvocationContext context, IProblemLocation problem, Collection<P> proposals);
		
		/* Copied from base method and adapted for tsuper ctor calls and role constructors. */
		@SuppressWarnings({"unchecked", "basecall", "inferredcallout", "decapsulation"})
		callin void getConstructorProposals(IInvocationContext context, IProblemLocation problem, Collection<P> proposals)
				throws CoreException
		{
			ICompilationUnit cu= context.getCompilationUnit();
	
			CompilationUnit astRoot= context.getASTRoot();
			ASTNode selectedNode= problem.getCoveringNode(astRoot);
			if (selectedNode == null) {
				return;
			}
	
			ITypeBinding targetBinding= null;
			List<Expression> arguments= null;
			IMethodBinding recursiveConstructor= null;
	
			int type= selectedNode.getNodeType();
			if (type == ASTNode.CLASS_INSTANCE_CREATION) {
				ClassInstanceCreation creation= (ClassInstanceCreation) selectedNode;

				ITypeBinding binding= creation.getType().resolveBinding();
				if (binding != null) {
					targetBinding= binding;
					arguments= creation.arguments();
				}
			} else if (type == ASTNode.SUPER_CONSTRUCTOR_INVOCATION) {
				ITypeBinding typeBinding= Bindings.getBindingOfParentType(selectedNode);
				if (typeBinding != null && !typeBinding.isAnonymous()) {
					targetBinding= typeBinding.getSuperclass();
					arguments= ((SuperConstructorInvocation) selectedNode).arguments();
				}
			} else if (type == ASTNode.CONSTRUCTOR_INVOCATION) {
				ITypeBinding typeBinding= Bindings.getBindingOfParentType(selectedNode);
				if (typeBinding != null && !typeBinding.isAnonymous()) {
					targetBinding= typeBinding;
					arguments= ((ConstructorInvocation) selectedNode).arguments();
					recursiveConstructor= ASTResolving.findParentMethodDeclaration(selectedNode).resolveBinding();
				}
//{ObjectTeams: similar to above but with different super class computation:
			} else if (type == ASTNode.TSUPER_CONSTRUCTOR_INVOCATION) {
				ITypeBinding typeBinding= Bindings.getBindingOfParentType(selectedNode);
				if (typeBinding != null && !typeBinding.isAnonymous()) {
					targetBinding= getTSuperClass(typeBinding);
					arguments= ((TSuperConstructorInvocation) selectedNode).getArguments();
				}
// SH}
			}

			if (selectedNode.getParent() instanceof EnumConstantDeclaration enumNode) {
				ITypeBinding typeBinding= Bindings.getBindingOfParentType(selectedNode);
				if (typeBinding != null && !typeBinding.isAnonymous()) {
					targetBinding= typeBinding;
					arguments= enumNode.arguments();
					selectedNode= enumNode;
				}
			}
			if (targetBinding == null) {
				return;
			}
//{ObjectTeams: instantiation requires the class:
			if (targetBinding.isRole() && targetBinding.isInterface())
				targetBinding = targetBinding.getClassPart();
// SH}
			ArrayList<IMethodBinding> similarElements= new ArrayList<>();
			for (IMethodBinding curr : targetBinding.getDeclaredMethods()) {
				if (curr.isConstructor() && recursiveConstructor != curr) {
					similarElements.add(curr); // similar elements can contain a implicit default constructor
				}
			}
	
			addParameterMissmatchProposals(context, problem, similarElements, selectedNode, arguments, proposals);
	
			if (targetBinding.isFromSource()) {
				ITypeBinding targetDecl= targetBinding.getTypeDeclaration();
	
				ICompilationUnit targetCU= ASTResolving.findCompilationUnitForBinding(cu, astRoot, targetDecl);
				if (targetCU != null) {
					String[] args= new String[] { ASTResolving.getMethodSignature( ASTResolving.getTypeSignature(targetDecl), getParameterTypes(arguments), false) };
					String label= Messages.format(CorrectionMessages.UnresolvedElementsSubProcessor_createconstructor_description, args);
					NewMethodCorrectionProposalCore core = new NewMethodCorrectionProposalCore(label, targetCU, selectedNode, arguments, targetDecl, IProposalRelevance.CREATE_CONSTRUCTOR);
					P coreToT= newMethodProposalToT(core, ConstructorProposal1);
					if (coreToT != null)
						proposals.add(coreToT);
				}
			}
		}
		
		static ITypeBinding getTSuperClass(ITypeBinding type) { // FIXME(SH): currently only first-level tsuper is used
			ITypeBinding[] superRoles = type.getSuperRoles();
			if (superRoles != null && superRoles.length > 0)
				return superRoles[0];
			return null;
		}
	}
}
