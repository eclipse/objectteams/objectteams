# Security Policy

This project implements the Eclipse Foundation Security Policy

* https://www.eclipse.org/security

## Supported Versions

These versions of Object Teams are currently being supported with security
updates.

| Version           | Released   | Supported          | 
| ----------------- | ---------- | ------------------ | 
| 2.8.2             | 2023-07-25 | :white_check_mark: | 
| < 2.8.1 (2020-06) | 2020-06-17 | :x:                | 

## Reporting a Vulnerability

Please report vulnerabilities to the Eclipse Foundation Security Team at
security@eclipse.org
